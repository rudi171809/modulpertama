import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import ListItem from './src/component/ListItem/ListItem';
import PlaceList from "./src/component/PlaceList/PlaceList";
import ImageList from "./src/asset/babastudio-logo.png";
export default class App extends React.Component {
  state = {
    text:"",
    button:[]
  };

  textChangeHandler = val => {
      this.setState({
        text : val
      });
  };


buttonSubmitHandler = () => {
   if(this.state.text.trim() === ""){
    return;
   } 
   this.setState(prevState=>{
     return{
       button:prevState.button.concat(prevState.text),
       image:ImageList
     }
   })

}

placeDeleteHandler = index => {
  this.setState(prevState => {
    return{
        button:prevState.button.filter((place,i)=>{
          return i !==index;
        })
    };
  });
};

render() {
  const textOutput = this.state.button.map((place,i)=>(
    // <Text key={i}>{place}</Text>
    <ListItem key={i} Text={place} onItemPressed={()=>this.placeDeleteHandler}/>
  ));
  return (
    
    <View style={{flex: 1, flexDirection: 'column', justifyContent:'flex-start', alignItems:'flex-start'}}>
     <View style={{flexDirection: 'column', paddingTop:26, justifyContent:'space-between', alignItems:'center'}}>
      <Text
        style={{marginRight:10, paddingTop:5}}
      >Nama : </Text>
      <TextInput
        style={{marginRight:10, width:200, borderColor: 'gray', borderWidth: 1}}
        placeholder='masukan Nama'
        value={this.state.text}
        onChangeText={this.textChangeHandler}
             
      />
       

      <Button
       title="Input"
       onPress = {this.buttonSubmitHandler}
      />
      </View>
   
      <PlaceList
          places={this.state.button}
          onItemDeleted={this.placeDeleteHandler}
      />
      </View>
     
    
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  listContainer:{
    width:"100%",
    margin:5,
    paddingTop:10
  }
});
