// import React from "react";
// import {View, StyleSheet,ScrollView } from "react-native";


// import ListItem from "../ListItem/ListItem";


// const placeList = props =>{
//     const placesOutput = props.places.map((place, i)=>(
//         <ListItem
//             key={i}
//             Text={place}
//             onItemPressed={() => props.onItemDeleted(i)}
//         />
//     ));
//     return (
//         <ScrollView style={styles.listContainer}>{placesOutput}</ScrollView>
//       );
//    // return <View style={StyleSheet.listContainer}>{placesOutput}</View>
// };

// const styles = StyleSheet.create({
//     listContainer:{
//         width:"100%"
//     }
// });


// export default placeList;


 
import React from 'react';
import { View, StyleSheet,ScrollView } from 'react-native';

import ListItem from '../ListItem/ListItem';

const placeList = props => {
    const placesOutput = props.places.map((place, i) => (
        // <ListItem key={i} placeName={place} />

               <ListItem
                    key={i}
                    placeName={place}
                    onItemPressed={() => props.onItemDeleted(i)}
                />
      ));
    return (
        // <View style={styles.listContainer}>{placesOutput}</View>
        <ScrollView style={styles.listContainer}>{placesOutput}</ScrollView>
        
    );
};

const styles = StyleSheet.create({
    listContainer: {
      width: "100%"
    }
});

export default placeList;