// import React from 'react';
// import {View, Text, StyleSheet,TouchableHighlight, Image} from 'react-native';

// const ListItem = (props) => (
//     <TouchableHighlight onPress={props.onItemPressed}>
//          <View style={styles.listItem}>
//              <Image style={styles.ImageItem} source={require('../../asset/babastudio-logo.png')}/>
//             <Text>{props.Text}</Text>
//         </View>
//     </TouchableHighlight>
       
// );


// const styles = StyleSheet.create({
//     listItem: {
//         width:"100%",
//         padding:"10",
//         backgroundColor:"#eee"

//     },
//     ImageItem: {
//         width: "30",
        
//     }
// });

// export default ListItem;

import React from 'react';
import { View, Text, StyleSheet,TouchableHighlight, Image } from 'react-native';

const listItem = (props) => (
   <TouchableHighlight onPress={props.onItemPressed}>
    <View style={styles.listItem}>
         <Image style={styles.ImageItem} source={require('../../asset/babastudio-logo.png')}/>
        <Text>{props.placeName}</Text>
    </View>
    </TouchableHighlight>
);

const styles = StyleSheet.create({
    listItem: {
        width: "100%",
        marginBottom: 5,
        padding: 10,
        backgroundColor: "#eee"
    }
});

export default listItem;